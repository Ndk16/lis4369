
# LIS4369

## Nicholas Koester

### Assignment 4 Requirements:

*Four Parts:*

1. Backward-engineered program with Python
2. Functions.py module
3. Main.py module
4. Questions

#### Assignment Screenshots:

*Data Analysis 2 (1/11):

![Data Analysis Screenshot (1/11)](img/DA_1.png "main module running")

*Data Analysis 2 (2/11):

![Data Analysis Screenshot (2/11)](img/DA_2.png "main module running")

*Data Analysis 2 (3/11):

![Data Analysis Screenshot (3/11)](img/DA_3.png "main module running")

*Data Analysis 2 (4/11):

![Data Analysis Screenshot (4/11)](img/DA_4.png "main module running")

*Data Analysis 2 (5/11):

![Data Analysis Screenshot (5/11)](img/DA_5.png "main module running")

*Data Analysis 2 (6/11):

![Data Analysis Screenshot (6/11)](img/DA_6.png "main module running")

*Data Analysis 2 (7/11):

![Data Analysis Screenshot (7/11)](img/DA_7.png "main module running")

*Data Analysis 2 (8/11):

![Data Analysis Screenshot (8/11)](img/DA_8.png "main module running")

*Data Analysis 2 (9/11):

![Data Analysis Screenshot (9/11)](img/DA_9.png "main module running")

*Data Analysis 2 (10/11):

![Data Analysis Screenshot (10/11)](img/DA_10.png "main module running")

*Data Analysis 2 (11/11):

![Data Analysis Screenshot (11/11)](img/DA_11.png "main module running")

*Skillset 10 (1/2):

![Skillset 10 screenshot (1/2)](img/ss10_1.png "Skillset 10 running")

*Skillset 10 (1/2):

![Skillset 10 screenshot (2/2)](img/ss10_2.png "Skillset 10 running")

*Skillset 11:

![Skillset 11 screenshot](img/ss11.png "Skillset 11 running")

*Skillset 12:

![Skillset 12 screenshot](img/ss12.png "Skillset 12 running")




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


