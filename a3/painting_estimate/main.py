#Nicholas Koester
#10/6/2021
#LIS4369
#A3 - main

import functions as f

def main():
    f.get_requirements()
    f.estimate_paint_cost()

if __name__ == "__main__":
    main()


