#Nicholas Koester
#11/7/2021
#LIS4369
#Skillset 14 - main

import functions as f

def main():
    f.get_requirements()
    f.write_read_file()

if __name__ == "__main__":
    main()