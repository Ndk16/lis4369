#Developer: Nicholas Koester
#LIS4369 - Skillset 1

def get_requirement():
    print("Square Feet to Acres")
    print("Program Requirements:\n"
        + "1. Research: number of sqaure feet to acre of land."
        + "2. Must use float data type for user input and calculation."
        + "3. Format and round conversion to two decimal places. ")

def calculate_sqft_to_acre():
    tract_size = 0.0
    acres = 0.0

    print("Input:")
    tract_size = float(input("Enter square feet: "))
    acres = tract_size / 43560

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3}".format(
        tract_size, "square feet =", acres, "acres"))