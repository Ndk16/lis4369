#Developer: Nicholas Koester
#LIS4369 - Skillset 1

import functions as f

def main():
    f.get_requirement()
    f.calculate_sqft_to_acre()


if __name__ == "__main__":
    main()