#Developer: Nicholas Koester
#LIS4369 - Skillset 2

def get_requirement():
    print("Miles Per Gallon")
    print("Program Requirements\n"
        + "1. Convert MPG.\n"
        + "2. Must use float data tyoe fir yser unput and calculation.\n"
        + "3. Format and round conversion to two decimal places\n")

def calculate_mpg():
    print("Input:")
    miles = float(input("Enter miles driven: "))
    gallons = float(input("Enter gallons of fuel used: "))

    mpg = miles / gallons

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f}".format
        (miles, "miles driven and", gallons, "gallons used =", mpg, "mpg"))