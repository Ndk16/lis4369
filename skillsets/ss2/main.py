#Developer: Nicholas Koester
#LIS4369 - Skillset 2

import functions as f

def main():
    f.get_requirement()
    f.calculate_mpg()


if __name__ == "__main__":
    main()