#Nicholas Koester
#11/3/2021
#LIS4369
#Skillset 10 - functions

def get_requirements():
    print("Using Dictionaries")
    print("\nProgram Requirements")
    print("1. Dictionaries: unordered key:value pairs")
    print("2. Dictionaries: an associative array")
    print("3. Any key in dictionary is associated to a value")
    print("4. Keys: must be immutable type and must be unique")
    print("5. Values: can be any data type and repeat")
    print("6. Create program that mirrors the following IPO")
    print("\tCreate empty dictionary")
    print("\tUse fname, lname, degree, major, gpa keys")

def get_diction():
    print("Input: \n")
    my_dictionary = {}
    my_dictionary = {
        "fname": input("First Name:"),
        "lname": input("Last Name:"),
        "degree": input("Degree:"),
        "major": input("Major:"),
        "gpa": input("GPA:")
    }
    print("Output:\n")
    print("Print my_dictionary")
    print(my_dictionary)

    print()

    print("Return view of dictionary's key-value pair")
    print(my_dictionary.keys())

    print()

    print("Return view object of all values in dictionary")
    print(my_dictionary.values())

    print()

    print("Print only first and last names using keys, built-in functions")
    print(my_dictionary["fname"], my_dictionary["lname"])

    print()

    print("Print only first and last names using get() functions")
    print(my_dictionary.get("fname"), my_dictionary.get("lname"))

    print() 

    print("Count number of items in dictionary")
    print(len(my_dictionary))

    print()

    print("Remove last dictionary item (popitem)")
    my_dictionary.popitem()
    print(my_dictionary)

    print() 

    print("Delete major from dictionary, using key:")
    del my_dictionary["major"]
    print(my_dictionary)

    print() 

    print("Return object type:")
    print(type(my_dictionary))

    print() 

    print("Delete all items from list:")
    my_dictionary.clear()
    print(my_dictionary)