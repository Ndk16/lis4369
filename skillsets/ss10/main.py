#Nicholas Koester
#11/3/2021
#LIS4369
#Skillset 10 - main

import functions as f

def main():
    f.get_requirements()
    f.get_diction()

if __name__ == "__main__":
    main()