#Nicholas Koester
#11/3/2021
#LIS4369
#Skillset 11 - functions

import random

def get_requirements():
    print("1. Get user beginning and ending values")
    print("2. Display 10 pseudo-random numbers between and including values")
    print("3. Must use int data types")
    print("4. Example 1: using range() and randint()")
    print("5. Example 2: using a list with range() and shuffle()")

def random_numbers():
    start = 0
    end = 0

    print("Input:")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")
    for count in range(10):
        print(random.randint(start,end), sep=", ", end=" ")

    print()

    print("\n Example 2: Using a list, with range() and shuffle() functions:")
    r = list(range(start, end +1))
    random.shuffle(r)
    for i in r:
        print(i, sep=", ", end=" ")

    print()


