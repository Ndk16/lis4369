#Nicholas Koester
#10/6/2021
#LIS4369
#Skillset 5 - Python Selection Structures FUNCTIONS

def get_requirements():
    print("Python Selection Structures")
    print("\nProgram Requirements:\n"
    + "1. Use Python selection structure.\n"
    + "2. Prompt user for two numbers, and a suitable operator.\n"
    + "3. Test for correct numeric operator.\n"
    + "4. Replicate display below.\n")

def get_input():
    print("Python Calculator")
    num1 = input("Enter num1: ")
    num2 = input("Enter num2: ")
    print("\nSuitable Operatores: +, -, *, /, // (interger division), % (modulo operator), ** (power)")
    operator = input("Enter operator: ")

    if num1.isnumeric() == True and num2.isnumeric() == True:
        n1 = float(num1)
        n2 = float(num2)
        operate(n1, n2, operator)
    else:
        print("Enter numbers not language")

def operate(num1, num2, operator):
    if operator == "+":
        num3 = num1 + num2
        print(num3)
    elif operator == "-":
        num3 = num1 - num2
        print(num3)
    elif operator == "*":
        num3 = num1 * num2
        print(num3)
    elif operator == "/":
        if( num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 / num2
            print(num3)
    elif operator == "//":
        if( num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 // num2
            print(num3)
    elif operator == "%":
        if( num2 == 0):
            print("Can't divide by zero")
        else:
            num3 = num1 % num2
            print(num3)
    elif operator == "**":
        num3 = num1 ** num2
        print(num3)
    else: 
        print("Illegal operator!")


