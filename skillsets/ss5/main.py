#Nicholas Koester
#10/6/2021
#LIS4369
#Skillset 5 - Python Selection Structures MAIN

import functions as f

def main():
    f.get_requirements()
    f.get_input()

if __name__ == "__main__":
    main()