#Nicholas Koester
#10/6/2021
#LIS4369
#Skillset 6 - Python Loops MAIN

import functions as f

def main():
    f.get_requirements()
    f.do_loops()

if __name__ == "__main__":
    main()