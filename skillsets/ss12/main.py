#Nicholas Koester
#11/3/2021
#LIS4369
#Skillset 12 - main

import functions as f

def main():
    f.get_requirements()
    f.temperature_conversion()

if __name__ == "__main__":
    main()



