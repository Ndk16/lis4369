#Nicholas Koester
#11/3/2021
#LIS4369
#Skillset 12 - functions

def get_requirements():
    print("1.  Program converts user entered temps into Farenheit or Celcius")
    print("2. Programs contintues to prompt until no longer requested")
    print("3. Note: upper or lower case letter permitted. Though, incorrect entries are not permitted.")
    print("4. Note: Program does not validate numeric data (optional requirement")

def temperature_conversion():
    temperature = 0.0
    choice = ''
    type = ''

    print("Input:")
    choice = input("Do you want to conver a temperature (y or n)? ").lower()

    print("\nOutput:")

    while(choice[0] == 'y'):
        type = input("Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\":").lower()

        if type[0] == 'f':
            temperature = float(input("Enter temperature in Fahrenheit: "))
            temperature = ((temperature -32)*5)/9
            print("Temperature in Celsius = " + str(temperature))
            choice = input("\nDo you want to convert to another temperature (y or n)?").lower()

        elif type[0] == 'c':
            temperature = float(input("Enter temperature in Celsius: "))
            temperature = (temperature * 9/5) + 32
            print("Temperature in Fahrenheit = " + str(temperature))
            choice = input("\nDo you want to convert to another temperature (y or n)?").lower()

        else:
            print("Incorrect entry. Please try again.")
            choice = input("\nDo you want to convert a temperature (y or n)?").lower()

    print("\nThan you for using our Temperature Conversion Program!")