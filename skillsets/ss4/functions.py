#Nicholas Koester
#10/6/2021
#LIS4369
#Skillset 4 - Calorie Percentage

def get_requirements():
    print("Calorie Percentage")
    print("Developer:Nicholas Koester\n")
    print("\nProgram Requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein.\n"
        + "2. Calculate percentages.\n"
        + "3. Must use float data types.\n"
        + "4. Format, right-align numbers, and round to two decimal places.\n")

def calculate_percentage():
    fat_grams = 0.0
    carb_grams = 0.0
    protein_grams = 0.0

    print("Input:")
    fat_grams = float(input("Enter total fat grams: "))
    carb_grams = float(input("Enter total carb grams: "))
    protein_grams = float(input("Enter total protein grams: "))

    fat_calories = fat_grams * 9
    carb_calories = carb_grams * 4
    protein_calories = protein_grams * 4

    calorie_total = fat_calories + carb_calories + protein_calories
    fat_percentage = (fat_calories/ calorie_total) * 100
    carb_percentage = (carb_calories / calorie_total) * 100
    protein_percentage = (protein_calories * calorie_total) * 100

    print_percent(fat_calories,carb_calories,protein_calories,fat_percentage,carb_percentage,protein_percentage)

def print_percent(fat_calories, carb_calories, protein_calories, fat_percentage, carb_percentage, protein_percentage):
    print("\nOutput:")
    print("Type" + "\tCalories" + "\tPercentage")
    print("{0} {1:,.2f} \t{2:,.2f}%".format("Fat\t", fat_calories, fat_percentage))
    print("{0} {1:,.2f} \t{2:,.2f}%".format("Carbs\t", carb_calories, carb_percentage))
    print("{0} {1:,.2f} \t{2:,.2f}%".format("Protein\t", protein_calories, protein_percentage))