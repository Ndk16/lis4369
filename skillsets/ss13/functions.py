#Nicholas Koester
#11/3/2021
#LIS4369
#Skillset 13 - functions

import math

def get_requirements():
    print("1. Program calculates sphere volume in liquid U.S gallons from user-entered diameter value in inches, and rounds to two decimal places."
        + "\n2. Must use Python's *built-in* PI and pow() capabilities"
        + "\n3. Program checks for non-integers and non-numeric values."
        + "\n4. Program continues to prompt for user entry until no longer requested, prompt accpets upper or lower case letters.\n")

def calculate_volume():
    while True:
        choice = str(input("Do you want to calculate a sphere volume (y or n)?"))
        print()
        while (choice == 'y' or choice == 'Y'):
            diameter = input("Please enter diameter in inches (integers only): ")
            if(diameter.isnumeric()):
                diameter2 = int(diameter)
                radius = diameter2/2

                volume = 4/3*math.pi*(pow(radius,3))
                gallons = volume/231

                finalvol = str(round(gallons, 2))
                print("\nSphere volume: " + finalvol + " liquid U.S. gallons\n")
                choice = str(input("Do you want to calculate another sphere volume (y or n)?"))
            else:
                print("\nNot valid integer!")
        else:
            print("\nThank you for using our Sphere Volumer Calculator!\n")
            break