#Nicholas Koester
#11/7/2021
#LIS4369
#Skillset 13 - main

import functions as f

def main():
    f.get_requirements()
    f.calculate_volume()

if __name__ == "__main__":
    main()