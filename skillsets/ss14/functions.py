#Nicholas Koester
#11/17/2021
#LIS4369
#Skillset 14 - calculator with error handling

import math as m

def get_requirements():
    print("Python Calculator with Error Handling\n")
    print("Program Requirements:\n")
    print("1. Program caculates two numbers, and rounds to two decimal places."
        + "\n2. Prompt user for two numbers, and a suitable operator."
        + "\n3. Use Python error handling to validate data."
        + "\n4. Test for correct arithmetic operator."
        + "\n5. Division by zero not permitted."
        + "\n6. Note: Program loops until correct input entered - numbers and arithmetic operator."
        + "\n7. Replicate display below.\n")

def getNum(prompt):
    while True:
        try:
            return float(input("\n" + prompt + " "))
        except ValueError:
            print("Not a number! Try again!")

def getOp():
    validOperators = ('+', '-', '*', '/', '//', '%', '**')
    while True:
        op = input("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power): ")
        try:
            validOperators.index(op)
            return op
        except ValueError:
            print("Invalid operator! Try again!")

def calc():
    num1 = getNum("Enter num1:")
    num2 = getNum("Enter num2:")
    op = getOp()
    sum = 0.0

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sum = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '*':
        sum = num1 ** num2
    elif op == '%':
        while True:
            try:
                sum = num1 % num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divid by zero! Re-enter num2:")

    elif op == '/':
        while True:
            try:
                sum = num1 / num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divid by zero! Re-enter num2:")

    elif op == '//':
        while True:
            try:
                sum = num1 // num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divid by zero! Re-enter num2:")

    print("\nAnswer is " + str(round(sum,2)))
    print()


