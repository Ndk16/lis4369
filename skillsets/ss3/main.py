#Developer: Nicholas Koester
#LIS4369 - Skillset 3

import functions as f

def main():
    f.get_requirement()
    f.calculate_percent()


if __name__ == "__main__":
    main()