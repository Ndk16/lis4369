#Developer: Nicholas Koester
#LIS4369 - Skillset 3

def get_requirement():
    print("IT/ICT Student Percentage\n")
    print("Program Requirements:\n"
        + "1. Find number of IT/ICT students in class,\n"
        + "2. Calculate IT/ICT Student Percentage.\n"
        + "3. Must use float data type (to faciliatate right-alignment).\n"
        + "4. Format, right-align numbers, and round to two decimal places.\n")

def calculate_percent():
    print("Input:")
    it_students = float(input("Enter number of IT students: "))
    ict_students = float(input("Enter number of ICT students: "))

    total_students = it_students + ict_students
    it_percent = (it_students / total_students) * 100
    ict_percent = (ict_students / total_students) * 100

    print("\nOutput:")
    print(f'Total Students: {total_students:>8.2f}')
    print(f'IT Students: {it_percent:>10.2f}%')
    print(f'ICT Students: {ict_percent:>9.2f}%\n')