#Nicholas Koester
#11/3/2021
#LIS4369
#A4 - functions

def print_requirements():
    print("Data Analysis 2")
    print("Program Requirements")
    print("1. Run demo.py")
    print("2. If errors, missing installations")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Test how to install any missing packages:")
    print("5. Create at least three functions that are called by the program: main(), get_requirements(), and data_analysis()")
    print("6. Display graph as per instructions in demo.py")