
# LIS4369

## Nicholas Koester

### Project 2 Requirements:

*Three Parts:*

1. Backward-engineered program with R
2. lis4369_p2 module
3. Output to file p2-output.txt


#### Assignment Screenshots:

*lis4369_p2 full view:

![4 panel Screenshot of lis4369](img/4panel.png "4 panel view of lis4369_p2")

*LIS4369_p2 graph 1:

![Main assignment Screenshot ](img/plot_1.png "Graph 1 of main assignment")

*LIS4369_p2 graph 2:

![Main assignment Screenshot ](img/plot_2.png "Graph 2 of main assignment")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


