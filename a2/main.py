#Nicholas Koester
#9/22/2021
#LIS4369
#A2 - main

import functions as f

def main():
    f.get_requirements()
    f.calculate_payroll()

if __name__ == "__main__":
    main()

