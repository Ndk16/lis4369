
# LIS4369

## Nicholas Koester

### Assignment 1 Requirements:

*Four Parts:*

1. Backward-engineered program with Python
2. Functions.py module
3. Main.py module
4. Questions

#### Assignment Screenshots:

*Payroll No Overtime:

![main module (IDLE) Screenshot](img/payrollNoOt.png "main module for no overtime calculation")

*Payroll with Overtime:

![main module (IDLE) Screenshot](img/payrollOt.png "main module for Overtime calculation")

*A2 Jupyter Notebook (1/4):

![Payroll_Calculator Jupyter Notebook](img/payroll_calc1.png "Payroll program in Jupyter Notebook")

*A2 Jupyter Notebook (2/4):

![Payroll_Calculator Jupyter Notebook](img/payroll_calc2.png "Payroll program in Jupyter Notebook")

*A2 Jupyter Notebook (3/4):

![Payroll_Calculator Jupyter Notebook](img/payroll_calc3.png "Payroll program in Jupyter Notebook")

*A2 Jupyter Notebook (4/4):

![Payroll_Calculator Jupyter Notebook](img/payroll_calc4.png "Payroll program in Jupyter Notebook")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


