
# LIS4369

## Nicholas Koester

### Assignment 5 Requirements:

*Four Parts:*

1. Backward-engineered program with Python
2. Functions.py module
3. Main.py module
4. Skillsets 13-15

#### Assignment Screenshots:

*Learn_to_use_r graph 1:

![Learn to us R Screenshot 1](img/learningR_1.png "Graph 1 of learning to use R")

*Learn_to_use_r graph 2:

![Learn to us R Screenshot 2](img/learningR_2.png "Graph 2 of learning to use R")

*LIS4369_a5 graph 1:

![Main assignment Screenshot ](img/a5_4panel_1.png "Graph 1 of main assignment")

*LIS4369_a5 graph 2:

![Main assignment Screenshot ](img/a5_4panel_2.png "Graph 2 of main assignment")

*Skillset 13:

![Skillset 13 screenshot ](img/ss13.png "Skillset 13 running")

*Skillset 14:

![Skillset 14 screenshot ](img/ss14.png "Skillset 14 running")

*Skillset 15:

![Skillset 15 screenshot](img/ss15.png "Skillset 15 running")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


