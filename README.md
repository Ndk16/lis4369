> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4369 - Extensible Enterprise Solutions

## Nicholas Koester

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create a1_tip_calculator application
    - Create a1 tip calculator Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Payroll Calculator with Overtime calculation
    - Create Payroll Calculator Jupyter Notebook

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - 

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Data Analysis 2 program
    - Create Data Analysis 2 Jupyter Notebooks
    - Provide screenshots of graphs
    - Skillset 10: Using Generator
    - Skillset 11: Random Number Generator
    - Skillset 12: temperature Conversion

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete R studio tutorial
    - Create and run LIS4369_a5
    - Skillset 13: Sphere Volume Calculator
    - Skillset 14: Calculator with Error Handling
    - Skillset 15: File Write/read

6. [P1 README.md](p1/README.md "My p1 README.md file")
    - Create Data Analysis application
    - Create Data analysis Jupyter Notebook
    - Provide screenshots of graphs
    - Skillset 7: Using Lists
    - Skillset 8: Using Tuples
    - Skillset 9: Using Sets 

7. [P2 README.md](p2/README.md "My p2 README.md file")
    - Create and run LIS4369_a5

