
# LIS4369

## Nicholas Koester

### Project 1 Requirements:

*Six Parts:*

1. Backward-engineered program with Python
2. Functions.py module
3. Main.py module
4. Program graphs
5. Jupyter Notebook
6. Skillsets 7 - 9

#### Assignment Screenshots:


*P1 Jupyter Notebook (1/2):

![Data Analysis 1 Jupyter Notebook 1/2](img/jp_1.png "Data Analysis program in Jupyter Notebook")

*P1 Jupyter Notebook (2/2):

![Data Analysis 1 Jupyter Notebook 2/2](img/jp_2.png "Data Analysis program in Jupyter Notebook")

*Skillset 7:

![Skillset 7 screenshot](img/ss7.png "Skillset 7 running")

*Skillset 8:

![Skillset 8 screenshot](img/ss8.png "Skillset 8 running")

*Skillset 9 1/2:

![Skillset 9 screenshot](img/ss9_1.png "Skillset 9 running")

*Skillset 9 2/2:

![Skillset 9 screenshot](img/ss9_2.png "Skillset 9 running")




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


