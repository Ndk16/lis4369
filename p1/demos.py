import datetime as dt 
import pandas_datareader as pdr 
import matplotlib.pyplot as plt
from matplotlib import style

start = dt.datetime(2010,1,1)
end = dt.datetime.now()

df = pdr.DataReader(["DJIA", "SP500"], "fred", start, end)

print("\nPrint number of records: ")
df.shape[0]

print(df.columns)

print("\nPrint data frames: ")
print(df)

print("\nPrint first five lines: ")
df1 = df.head(3)
print(df1)

print("\nPrint last five lines: ")
df3 = df.tail(3)
print(df3)

print("\nPrint first 2 lines: ")


print("\nPrint last 2 lines: ")


style.use('ggplot')

df['DJIA'].plot()
df['SP500'].plot()
pit.legend()
pit.show()





