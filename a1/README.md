
# LIS4369

## Nicholas Koester

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. bitbucket repo links:
    a) this assignment and
    b) the completed tutorial (bitbucketstationlocation)

> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator application running (IDLE):

![a1_tip_calculator (IDLE) Screenshot](img/a1_tip_calc_idle.png "a1_tip_calculator running in IDLE")

*Screenshot of a1_tip_calculator application running (Visual Studio Code):

![a1_tip_calculator (VSC) Screenshot](img/a1_tip_calc_vsc.png "a1_tip_calculator running in VSC")

*A1 Jupyter Notebook:

![a1_tip_calculator Jupyter Notebook](img/a1_tip_calc_jn.png "a1_tip_calculator running in Jupyter Notebook")





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


